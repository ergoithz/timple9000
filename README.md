# Timple 9000

## Setup (Arch Linux)

1. `npm i`
2. Install Android SDK:
   - [android-emulator](http://aur.archlinux.org/packages/android-emulator)<sup><small>AUR</small></sup>
   - [android-sdk-build-tools](https://aur.archlinux.org/packages/android-sdk-build-tools)<sup><small>AUR</small></sup>
   - [android-x86-64-system-image-29](https://aur.archlinux.org/packages/android-x86-64-system-image-29)<sup><small>AUR</small></sup>
3. Configure Android SDK:
   - Create AVD: `/opt/android-sdk/tools/android create avd --name android-29 --package 'system-images;android-29;default;x86_64'`

## Build (all targets)

```sh
npm run build
```

## Test (browser)

```sh
npm start
```

## Test (android)

```sh
npm run start-android
```