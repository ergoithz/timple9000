
const
  {rm, mkdir, copyFile} = require('fs').promises,
  {basename, resolve} = require('path'),
  fg = require('fast-glob'),
  {waitPromise} = require('./utils'),
  target = resolve(__dirname, '../dist'),
  app = {
    source: resolve(__dirname, '../platforms'),
    target: target,
    files: {
      'android': 'android/app/build/outputs/**/*.apk',
    },
    async process() {

      for (const [category, glb] of Object.entries(this.files)){
        await mkdir(`${this.target}/${category}`, {recursive: true});
        for await (const path of fg.stream(glb, {cwd: this.source, onlyFiles: true})) {
          await copyFile(`${this.source}/${path}`, `${this.target}/${category}/${basename(path)}`);
        }
      }
    },
  },
  process = async () => {
    await rm(target, {recursive: true, force: true});
    await app.process();
  };

waitPromise(process());
