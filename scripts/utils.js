/**
 * Forces NodeJS to be alive until promise resolves.
 * @param {Promise} promise
 * @returns {Promise}
 */
function waitPromise(promise){
  function wait() {
    if (error) throw error;
    if (!finished) setTimeout(() => wait(), 100);
  }
  let
    finished,
    error;
  promise.then(
    () => {finished = true},
    (e) => {error = e},
  );
  wait();
  return promise;
}

exports.waitPromise = waitPromise;
