
const
  {rm, mkdir, copyFile, readFile, writeFile} = require('fs').promises,
  {dirname, resolve, relative} = require('path'),
  fg = require('fast-glob'),
  sharp = require('sharp'),
  {waitPromise} = require('./utils'),
  source = resolve(__dirname, '../public'),
  target = resolve(__dirname, '../www'),
  app = {
    source: source,
    target: target,
    exclude: [
      'cordova.js',
    ],
    files: [
      '*.*',
      'icomoon/style.css',
      'icomoon/fonts/*',
      'fonts/*',
      'icons/*',
    ],
    replaces: [
      ['index.html', /<!--cordova-placeholder-->/g, '<script src="cordova.js"></script>'],
    ],
    async process() {
      await mkdir(this.target, {recursive: true});
      for (const glb of this.files) {
        for await (const path of fg.stream(glb, {cwd: this.source, onlyFiles: true, ignore: this.exclude})) {
          await mkdir(dirname(`${this.target}/${path}`), {recursive: true});
          await copyFile(`${this.source}/${path}`, `${this.target}/${path}`);
        }
      }
      for (const [name, regex, replacement] of this.replaces) {
        const data = await readFile(`${this.target}/${name}`, {encoding: 'utf8'});
        await writeFile(`${this.target}/${name}`, data.replace(regex, replacement));
      }
    }
  },
  icons = {
    source: resolve(__dirname, '../icons'),
    target: `${source}/icons`,
    android: {
      files: [
        'android-background.svg',
        'android-foreground.svg',
        'android.svg',
      ],
      sizes: {
        ldpi: 36,
        mdpi: 48,
        hdpi: 72,
        xhdpi: 96,
        xxhdpi: 144,
        xxxhdpi: 192,
      },
    },
    async process(){
      await mkdir(this.target, {recursive: true});
      for (const [mode, size] of Object.entries(this.android.sizes)) {
        for (const glb of this.android.files) {
          for await (const path of fg.stream(glb, {cwd: this.source, onlyFiles: true, absolute: true})) {
            const dest = path.replace(/\.svg$/, `-${mode}.png`);
            await sharp(path)
              .resize(size, size)
              .png()
              .toFile(dest);
            await copyFile(dest, `${this.target}/${relative(this.source, dest)}`)
          }
        }
      }
    },
  },
  process = async () => {
    await rm(target, {recursive: true, force: true});
    await icons.process();
    await app.process();
  };

waitPromise(process());